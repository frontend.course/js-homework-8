const paragraphs = document.querySelectorAll("p")
paragraphs.forEach((item) => item.style.backgroundColor = "#ff0000")

const optionList = document.getElementById("optionsList")
console.log(optionList)

console.log(optionList.parentElement)
const optionListChildNodes = optionList.childNodes
optionListChildNodes.forEach((node) => {
    console.log(node.nodeType, node.nodeName)
})

const testParagraph = document.querySelector("#testParagraph")
const paragraph = document.createElement("p")
paragraph.innerText = "This is a paragraph"

testParagraph.append(paragraph)

const mainHeader = document.querySelector(".main-header")
const mainHeaderChildren = mainHeader.children
Array.from(mainHeaderChildren).forEach((item) => {
    item.classList.add("nav-item")
})

const sectionTitles = document.querySelectorAll(".section-title")

sectionTitles.forEach((item) => {
    item.classList.remove("section-title")
})